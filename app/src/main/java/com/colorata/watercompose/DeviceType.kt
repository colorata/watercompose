package com.colorata.watercompose

import androidx.compose.runtime.compositionLocalOf

enum class DeviceType {
    Mobile,
    Wear,
    Desktop
}

val LocalDeviceType = compositionLocalOf { DeviceType.Mobile }