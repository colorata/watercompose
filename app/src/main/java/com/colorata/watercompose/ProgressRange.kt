package com.colorata.watercompose

import androidx.annotation.FloatRange

@FloatRange(from = 0.0, to = 1.0, toInclusive = true)
annotation class ProgressRange