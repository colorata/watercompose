package com.colorata.watercompose.ui.components

import androidx.annotation.FloatRange
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Slider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.clipRect
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.colorata.watercompose.ProgressRange

@Composable
fun SemiLayout(
    topLayout: @Composable () -> Unit,
    bottomLayout: @Composable () -> Unit,
    @ProgressRange progress: Float,
    modifier: Modifier = Modifier
) {
    Box(modifier) {
        Box(modifier = Modifier.drawWithContent {
            clipRect(bottom = size.height * (1f - progress)) {
                println(size.height * (1f - progress))
                this@drawWithContent.drawContent()
            }
        }) {
            topLayout()
        }
        Box(modifier = Modifier.drawWithContent {
            clipRect(top = size.height * (1f - progress)) {
                this@drawWithContent.drawContent()
            }
        }) {
            bottomLayout()
        }

    }
}

@Preview
@Composable
fun SemiLayoutPreview() {
    var progress by remember { mutableStateOf(0f) }
    Box(modifier = Modifier.border(1.dp, Color.Red)) {
        SemiLayout(topLayout = {
            Box(
                modifier = Modifier
                    .height(100.dp)
                    .fillMaxWidth()
                    .background(Color.Black)
            )
        }, bottomLayout = {
            Box(
                modifier = Modifier
                    .height(100.dp)
                    .fillMaxWidth()
                    .background(Color.White)
            )
        }, progress = progress)
        Slider(value = progress, onValueChange = {
            progress = it
        }, valueRange = 0f..1f)
    }
}