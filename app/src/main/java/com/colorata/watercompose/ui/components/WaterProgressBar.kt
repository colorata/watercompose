package com.colorata.watercompose.ui.components

import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.wear.compose.foundation.CurvedLayout
import androidx.wear.compose.foundation.curvedRow
import com.colorata.watercompose.ProgressRange
import kotlin.math.PI
import kotlin.math.sin

private const val alphaOffset = 0.3f
private const val waveAmplitude = 50f

@Composable
private fun progressBarColor(@ProgressRange progress: Float): Color {
    val alpha = remember(progress) { minOf(progress + alphaOffset, 0.8f) }
    val primaryColor = MaterialTheme.colorScheme.primary

    return primaryColor.copy(alpha = alpha)
}

@Composable
fun WaterWaveProgressBar(
    @ProgressRange progress: Float,
    modifier: Modifier = Modifier
) {
    val infiniteTransition = rememberInfiniteTransition()

    val widthOffset by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = (2f * PI * waveAmplitude).toFloat(),
        animationSpec = infiniteRepeatable(
            animation = tween(1000, easing = LinearEasing),
            repeatMode = RepeatMode.Restart
        )
    )

    val color = progressBarColor(progress)
    Canvas(modifier = modifier.fillMaxSize()) {

        repeat(size.width.toInt()) { x ->
            val xWithOffset = x.toFloat() + widthOffset
            drawLine(
                color,
                Offset(
                    x.toFloat(),
                    calculateSinOffset(
                        xWithOffset, size.height, 1f - progress
                    )
                ),
                Offset(x.toFloat(), size.height)
            )
        }
    }
}

@Preview
@Composable
private fun WaterWaveProgressBarPreview() {
    var progress by remember { mutableStateOf(0f) }
    Box(modifier = Modifier.border(1.dp, Color.Red)) {
        WaterWaveProgressBar(progress)
        Slider(value = progress, onValueChange = {
            progress = it
        }, valueRange = 0f..1f)
    }
}

private fun calculateSinOffset(
    currentWidth: Float,
    height: Float,
    @ProgressRange progress: Float
): Float {
    return height * progress + (sin(currentWidth / waveAmplitude) - 1f) * waveAmplitude
}