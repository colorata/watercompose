package com.colorata.watercompose.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import com.colorata.watercompose.DeviceType
import com.colorata.watercompose.LocalDeviceType
import com.colorata.watercompose.ui.components.SemiLayout
import com.colorata.watercompose.ui.components.WaterWaveProgressBar
import com.colorata.watercompose.ui.screens.mobile.BaseWaterMobileLayout
import com.colorata.watercompose.ui.screens.wear.BaseWaterWearLayout
import com.colorata.watercompose.viewmodel.WaterScreenViewModel
import com.colorata.watercompose.viewmodel.leftWaterMillis
import com.colorata.watercompose.viewmodel.waterProgress
import com.colorata.watercompose.ui.theme.WaterComposeTheme
import kotlinx.coroutines.launch

@Composable
fun WaterScreen(modifier: Modifier = Modifier) {
    val vm = viewModel<WaterScreenViewModel>()
    val scope = rememberCoroutineScope()
    Box(modifier = modifier.fillMaxSize()) {
        WaterWaveProgressBar(vm.waterProgress)
        WaterLayout(
            leftWaterMillis = vm.leftWaterMillis.toInt(),
            currentWaterMillis = vm.currentWaterMillis.value.toInt(),
            progress = vm.waterProgress,
            onButtonClick = {
                scope.launch {
                    vm.updateWaterMillis()
                }
            })
    }
}

@Composable
private fun WaterLayout(
    leftWaterMillis: Int,
    currentWaterMillis: Int,
    progress: Float,
    onButtonClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    SemiLayout(topLayout = {
        BaseWaterLayout(
            leftWaterMillis = leftWaterMillis,
            leftWaterColor = MaterialTheme.colorScheme.primary,
            currentWaterMillis = currentWaterMillis,
            currentWaterColor = MaterialTheme.colorScheme.primary,
            onButtonClick = onButtonClick
        )
    }, bottomLayout = {
        BaseWaterLayout(
            leftWaterMillis = leftWaterMillis,
            leftWaterColor = MaterialTheme.colorScheme.onPrimary,
            currentWaterMillis = currentWaterMillis,
            currentWaterColor = MaterialTheme.colorScheme.onPrimary,
            onButtonClick = onButtonClick
        )
    }, progress = progress, modifier)
}

@Composable
private fun BaseWaterLayout(
    leftWaterMillis: Int,
    leftWaterColor: Color,
    currentWaterMillis: Int,
    currentWaterColor: Color,
    onButtonClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    when (LocalDeviceType.current) {
        DeviceType.Mobile, DeviceType.Desktop ->
            BaseWaterMobileLayout(
                leftWaterMillis = leftWaterMillis,
                leftWaterColor = leftWaterColor,
                currentWaterMillis = currentWaterMillis,
                currentWaterColor = currentWaterColor,
                onButtonClick = onButtonClick,
                modifier = modifier
            )

        DeviceType.Wear ->
            BaseWaterWearLayout(
                leftWaterMillis = leftWaterMillis,
                leftWaterColor = leftWaterColor,
                currentWaterMillis = currentWaterMillis,
                currentWaterColor = currentWaterColor,
                onButtonClick = onButtonClick,
                modifier = modifier
            )
    }
}

@Preview
@Composable
private fun WaterScreenPreview() {
    WaterComposeTheme {
        WaterScreen()
    }
}