package com.colorata.watercompose.ui.screens.mobile

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color

@Composable
fun BaseWaterMobileLayout(
    leftWaterMillis: Int,
    leftWaterColor: Color,
    currentWaterMillis: Int,
    currentWaterColor: Color,
    onButtonClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        modifier.fillMaxSize(),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = "Осталось:\n${leftWaterMillis}ml", color = leftWaterColor)
        Text(text = "Выпито:\n${currentWaterMillis}ml", color = currentWaterColor)
        Button(onClick = { onButtonClick() }) {
            Text(text = "+250ml")
        }
    }
}