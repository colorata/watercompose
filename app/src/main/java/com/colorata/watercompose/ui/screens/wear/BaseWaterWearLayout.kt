package com.colorata.watercompose.ui.screens.wear

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.wear.compose.foundation.AnchorType
import androidx.wear.compose.foundation.CurvedDirection
import androidx.wear.compose.foundation.CurvedLayout
import androidx.wear.compose.material.curvedText

@Composable
fun BaseWaterWearLayout(
    leftWaterMillis: Int,
    leftWaterColor: Color,
    currentWaterMillis: Int,
    currentWaterColor: Color,
    onButtonClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    CurvedLayout(
        modifier = Modifier.fillMaxSize()
    ) {
        curvedText("${leftWaterMillis}ml", color = leftWaterColor)
    }
    CurvedLayout(
        modifier = Modifier.fillMaxSize().rotate(180f),
        angularDirection = CurvedDirection.Angular.CounterClockwise,
    ) {
        curvedText("${currentWaterMillis}ml", color = currentWaterColor)
    }
    Column(
        modifier.fillMaxSize(),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Button(onClick = { onButtonClick() }) {
            Text(text = "+250ml")
        }
    }
}