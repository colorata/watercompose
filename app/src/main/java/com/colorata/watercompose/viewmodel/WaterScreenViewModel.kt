package com.colorata.watercompose.viewmodel

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.EaseInOutBounce
import androidx.compose.animation.core.EaseOutBack
import androidx.compose.animation.core.EaseOutBounce
import androidx.compose.animation.core.tween
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

private const val waterStepMillis = 250f
private const val maxWaterMillis = 3000f

class WaterScreenViewModel : ViewModel() {
    val currentWaterMillis = Animatable(0f)

    suspend fun updateWaterMillis() {
        if (currentWaterMillis.targetValue < 3000f) {
            currentWaterMillis.animateTo(
                currentWaterMillis.targetValue + waterStepMillis,
                tween(1000, easing = EaseOutBounce)
            )
        }
    }
}

val WaterScreenViewModel.waterProgress: Float
    get() = currentWaterMillis.value / maxWaterMillis

val WaterScreenViewModel.leftWaterMillis: Float
    get() = maxWaterMillis - currentWaterMillis.value